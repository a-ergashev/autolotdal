﻿using System;
using System.Data;
using AutoLotDAL.Models;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace AutoLotDAL.DataOperations
{
    public class InventoryDAL
    {
        private readonly string _connectionString;
        private SqlConnection _connection;
        public InventoryDAL()
            : this(@"Data source=(localdb)\mssqllocaldb;Initial Catalog=AutoLot;Integrated Security=True;") { }
        public InventoryDAL(string connectionString)
            => _connectionString = connectionString;
        public void OpenConnection()
        {
            _connection = new SqlConnection(_connectionString);
            _connection.Open();
        }
        public void CloseConnection()
        {
            if (_connection.State != ConnectionState.Closed)
                _connection.Close();
        }
        public List<Car> GetAllInventory()
        {
            OpenConnection();

            List<Car> inventory = new List<Car>();
            string sql = "SELECT * FROM Inventory";
            using (SqlCommand command = new SqlCommand(sql, _connection))
            {
                command.CommandType = CommandType.Text;
                SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                while (reader.Read())
                {
                    inventory.Add(new Car
                    {
                        CarId = reader.GetInt32(0),
                        Make = reader.GetString(1),
                        Color = reader.GetString(2),
                        PetName = reader.GetString(3)
                    });
                }
                reader.Close();
            }
            return inventory;
        }
        public Car GetCar(int id)
        {
            OpenConnection();
            Car car = new Car();
            string sql = $"SELECT * FROM Inventory WHERE CarId = {id}";
            using (SqlCommand command = new SqlCommand(sql, _connection))
            {
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();
                car.CarId = reader.GetInt32(0);
                car.Make = reader.GetString(1);
                car.Color = reader.GetString(2);
                car.PetName = reader.GetString(3);
            }

            return car;
        }
        public void InsertAuto(string make, string color, string petName)
        {
            OpenConnection();

            string sql = $"INSERT INTO Inventory(Make, Color, PetName) " +
                $"VALUES ({make}, {color}, {petName})";
            using (SqlCommand command = new SqlCommand(sql, _connection))
            {
                command.CommandType = CommandType.Text;
                command.ExecuteNonQuery();
            }
            CloseConnection();
        }
        public void InsertAuto(Car car)
        {
            OpenConnection();

            string sql = "INSERT INTO Inventory(Make, Color, PetName) " +
                "VALUES (@Make, @Color, @PetName)";
            using (SqlCommand command = new SqlCommand(sql, _connection))
            {
                command.CommandType = CommandType.Text;
                SqlParameter make = new SqlParameter()
                {
                    ParameterName = "@Make",
                    Value = car.Make,
                    SqlDbType = SqlDbType.Char,
                    Size = 10
                };
                command.Parameters.Add(make);

                SqlParameter color = new SqlParameter()
                {
                    ParameterName = "@Color",
                    Value = car.Color,
                    SqlDbType = SqlDbType.Char,
                    Size = 10
                };
                command.Parameters.Add(color);

                SqlParameter name = new SqlParameter()
                {
                    ParameterName = "@PetName",
                    Value = car.PetName,
                    SqlDbType = SqlDbType.Char,
                    Size = 10
                };
                command.Parameters.Add(name);
                command.ExecuteNonQuery();
            }
            CloseConnection();
        }
        public void DeleteCar(int id)
        {
            OpenConnection();

            string sql = $"DELETE FROM Inventory WHERE CarId = {id}";
            using (SqlCommand command = new SqlCommand(sql, _connection))
            {
                try
                {
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                }
                catch (SqlException)
                {
                    Exception error = new Exception("Sorry, the car is on the order!");
                    throw error;
                }
            }
            CloseConnection();
        }
        public void UpdateCarPetName(int id, string petName)
        {
            OpenConnection();

            string sql = $"UPDATE Inventory SET PetName = '{petName}' WHERE CarId = '{id}'";
            using (SqlCommand command = new SqlCommand(sql, _connection))
            {
                command.CommandType = CommandType.Text;
                command.ExecuteNonQuery();
            }

            CloseConnection();
        }
        public string LookUpPetName(int carId)
        {
            OpenConnection();
            string carPetName;
            // Establish name of stored proc.
            using (SqlCommand command = new SqlCommand("GetPetName", _connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                // Input param.
                SqlParameter param = new SqlParameter
                {
                    ParameterName = "@carId",
                    SqlDbType = SqlDbType.Int,
                    Value = carId,
                    Direction = ParameterDirection.Input
                };
                command.Parameters.Add(param);
                // Output param.
                param = new SqlParameter
                {
                    ParameterName = "@petName",
                    SqlDbType = SqlDbType.Char,
                    Size = 10,
                    Direction = ParameterDirection.Output
                };
                command.Parameters.Add(param);
                // Execute the stored proc.
                command.ExecuteNonQuery();
                // Return output param.
                carPetName = (string)command.Parameters["@petName"].Value;
                CloseConnection();
            }
            return carPetName;
        }
        public void ProcessCreditRisk(bool throwex, int id)
        {
            OpenConnection();
            string fname = null, lname = null;
            string sqlSelect = $"SELECT * FROM Customers WHERE CustID = {id}";
            SqlCommand cmdSelect = new SqlCommand(sqlSelect, _connection);
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                reader.Read();
                if (reader.HasRows)
                {
                    fname = reader.GetString(1);
                    lname = reader.GetString(2);
                }
            }

            string sqlDelete = $"DELETE FROM Customers WHERE CustId = {id}";
            SqlCommand cmdDelete = new SqlCommand(sqlDelete, _connection);

            string sqlInsert = $"INSERT INTO CreditRisks (FirstName, LastName) VALUES ('{fname}', '{lname}')";
            SqlCommand cmdInsert = new SqlCommand(sqlInsert, _connection);
            SqlTransaction tx = null;
            try
            {
                tx = _connection.BeginTransaction();
                cmdDelete.Transaction = tx;
                cmdInsert.Transaction = tx;
                cmdDelete.ExecuteNonQuery();
                cmdInsert.ExecuteNonQuery();
                if (throwex)
                {
                    throw new DataException();
                }
                tx.Commit();
            }
            catch
            {
                Console.WriteLine("The transaction failed as a collective unit");
                tx.Rollback();
            }
            finally
            {
                CloseConnection();
            }
        }
    }
}
