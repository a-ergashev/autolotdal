﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace AutoLotDAL.BulkImport
{
    public static class ProcessBulkImport
    {
        private static string _connectionString = @"Data source=(localdb)\mssqllocaldb;Initial catalog=AutoLot;Integrated security=true";
        private static SqlConnection _connection = null;

        private static void OpenConnection()
        {
            _connection = new SqlConnection(_connectionString);
            _connection.Open();
        }

        private static void CloseConnection()
        {
            if (_connection?.State != ConnectionState.Closed)
            {
                _connection?.Close();
            }
        }

        public static void ExecuteBulkImport<T>(IEnumerable<T> records, string tableName)
        {
            OpenConnection();
            using (SqlConnection connection = _connection)
            {
                SqlBulkCopy sqlBulk = new SqlBulkCopy(connection)
                {
                    DestinationTableName = tableName
                };

                MyDataReader<T> reader = new MyDataReader<T>() { Records = records.ToList() };
                try
                {
                    sqlBulk.WriteToServer(reader);
                }
                finally
                {
                    connection.Close();
                }
            }
            CloseConnection();
        }
    }
}
