﻿using System.Collections.Generic;
using System.Data;

namespace AutoLotDAL.BulkImport
{
    internal interface IMyDataReader<T> : IDataReader
    {
        List<T> Records { get; set; }
    }
}
